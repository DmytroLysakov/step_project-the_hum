import { navbarScroll } from './modules/navbar.js';
import { tabs } from './modules/our_services.js';
import { filter } from './modules/our_work.js';
import { slider } from './modules/slider.js';
import { gallery } from './modules/gallery.js';

navbarScroll();
tabs();
filter();
slider();
gallery();

// let menu = [`<img src="../../img/slider/Death.png" alt="employee photo">`,
// 	`<img src="../../img/slider/Death.png" alt="employee photo">`,
// 	`<img src="../../img/slider/Death.png" alt="employee photo">`,
// 	`<img src="../../img/slider/Death.png" alt="employee photo">`,];

// const swiper = new Swiper('.swiper', {
// 	loop: true,

// 	pagination: {
// 		el: '.feedback__slider-photos',
// 		type: 'bullets',
// 		bulletClass: 'slider-photo',
// 		bulletActiveClass: 'active-slider',
// 		clickable: true,
// 		clickableClass: 'feedback__slider-photos',
// 		//функція яка рендерить буллети для пагінації
// 		renderBullet: function (index, className) {
// 			return '<button class="' + className + '">' + (menu[index]) + '</button>';
// 		},
// 	},

// 	navigation: {
// 		nextEl: '.right-btn',
// 		prevEl: '.left-btn',
// 	},
// });
// //для вибору початкового слайду swiper.slideTo
// //(номер слайду; шидкість(будь яка, бо при false вирубає кнопки); подія переходу)
// swiper.slideTo(3, 100, false);
