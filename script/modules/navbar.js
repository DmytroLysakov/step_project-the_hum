export function navbarScroll() {
	window.addEventListener('scroll', navbarScroll);
	function navbarScroll() {
		let header = document.querySelector('.navbar')
		if (window.scrollY >= 10) {
			header.classList.add('navbar--on-scroll')
		} else if (window.scrollY === 0) {
			header.classList.remove('navbar--on-scroll')
		}
	};
}
