export function tabs() {
	let tabsList = document.querySelector('.tabs__list');

	tabsList.addEventListener('click', showTab);

	function showTab(e) {
		if (e.target.closest('.tabs__item')) {
			let targetItem = e.target.closest('.tabs__item');
			let items = tabsList.querySelectorAll('[data-tab-item]');
			let content = document.querySelectorAll('[data-tab-content]');

			items.forEach(item => item.classList.remove('tabs__item--active'));
			targetItem.classList.add('tabs__item--active')
			content.forEach(tab => {
				if (tab.dataset.tabContent === targetItem.dataset.tabItem) {
					tab.classList.add('content__item--active');
				} else {
					tab.classList.remove('content__item--active');
				}
			});
		}
	}
}
