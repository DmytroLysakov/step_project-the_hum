export function slider() {
	let menu = [`<img src="../img/slider/Death.png" alt="Death photo">`,
		`<img src="img/slider/PumpQuinn2.png" alt="PumpQuinn2 photo">`,
		`<img src="img/slider/Harriet.png" alt="employee photo">`,
		`<img src="img/slider/Jerry.png" alt="employee photo">`,];

	const swiper = new Swiper('.swiper', {
		loop: true,
		pagination: {
			el: '.slider__pagination',
			type: 'bullets',
			bulletClass: 'slider__photo',
			bulletActiveClass: 'slider__photo--active',
			clickable: true,
			clickableClass: 'slider__pagination',
			renderBullet: function (index, className) {
				return '<button class="' + className + '">' + (menu[index]) + '</button>';
			},
		},
		navigation: {
			nextEl: '.slider__btn--next',
			prevEl: '.slider__btn--prev',
		},
	});
	swiper.slideTo(1);
}